EECS 481 HW 2 Submission

Screenshots are in the 'Screenshots' folder. One contains the basic application
running on the simulator. The other contains the app after playing around with
the APIs running on my phone. 

When you click on the button it shows how many times it's been clicked. It also
updates the text at the top to show the number of touches and the size and 
location of each. 
