package com.eecs481.aareshb.hw2;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity implements OnTouchListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        findViewById(R.id.button1).setOnTouchListener(this);
    }

    private int buttonClicks = 0;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
    public void modifyText(View view) {
    	buttonClicks++;
    	((Button)findViewById(R.id.button1)).setText("Button has been clicked: " + buttonClicks + " times");
    	
    }

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		String output = "";
		
		output += "Num: " + event.getPointerCount();
		output += "\nSize: ";
		
		for(int i = 0; i < event.getPointerCount(); i++) 
			output += event.getSize(i) + ", ";
		
		output += "\nLoc: ";
		
		for(int i = 0; i < event.getPointerCount(); i++)
			output += "(" + event.getX(i) + "," + event.getY(i) + ") ";
		
		
		((TextView)findViewById(R.id.textView1)).setText(output);
    	
		return false;
	}
}
